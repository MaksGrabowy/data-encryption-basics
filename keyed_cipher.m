clear all;
close all;

s = rng;
key = randi(255,1,20);

fkey = fopen("inputs/key.txt","w","n","UTF-8");

key_file = fwrite(fkey,key);
fclose(fkey);

%%
key_generated = fopen("inputs/key.txt","r","n","UTF-8");
key_read = fread(key_generated);
fclose(key_generated);

file = fopen("inputs/original.txt","r","n","UTF-8");
original_file = fread(file);
fclose(file);


key = 3;
ascii_original = double(original_file);

for i = 1:length(ascii_original)
    index = mod(i,length(key_read))+1;
    ascii_cipher(i) = mod(ascii_original(i)+key_read(index),255);
end

file_result = fopen("outputs/result_text_better.txt","w","n","UTF-8");
for i = 1:length(ascii_cipher)
    fwrite(file_result,ascii_cipher(i));
end

fclose(file_result);

%%
file_decipher = fopen("outputs/result_text_better.txt","r","n","UTF-8");
deciphered_file = fread(file_decipher);

ascii_decipher = double(deciphered_file);

for i = 1:length(ascii_decipher)
    index = mod(i,length(key_read))+1;
    deciphered_result(i) = mod(ascii_decipher(i)-key_read(index),255);
end

file_result = fopen("outputs/decipher_better.txt","w","n","UTF-8");
for i = 1:length(deciphered_result)
    fwrite(file_result,deciphered_result(i));
end

fclose(file_result);

%%
figure()
histogram(ascii_original,255,'BinWidth',1)

figure()
histogram(ascii_cipher,255,'BinWidth',1)

figure()
histogram(deciphered_result,255,'BinWidth',1)


clear all
close all
file = fopen("inputs/original.txt","r","n","UTF-8");
original_file = fread(file);
fclose(file);
key = 30;
ascii_original = double(original_file);

for i = 1:length(ascii_original)
    ascii_cipher(i) = mod(ascii_original(i)+key,255);
end

file_result = fopen("outputs/result_text.txt","w","n","UTF-8");
for i = 1:length(ascii_cipher)
    fwrite(file_result,ascii_cipher(i));
end

fclose(file_result);

%%
file_decypher = fopen("outputs/result_text.txt","r","n","UTF-8");
decyphered_file = fread(file_decypher);

ascii_decypher = double(decyphered_file);

for i = 1:length(ascii_decypher)
    deciphered_result(i) = mod(ascii_decypher(i)-key,255);
end

file_result = fopen("outputs/decipher.txt","w","n","UTF-8");
for i = 1:length(deciphered_result)
    fwrite(file_result,deciphered_result(i));
end

fclose(file_result);

%%
figure(1)
subplot(1,3,1)
histogram(ascii_original,255,'BinWidth',1)
title("Original")

subplot(1,3,2)
histogram(ascii_cipher,255,'BinWidth',1)
title("Ciphered")

subplot(1,3,3)
histogram(deciphered_result,255,'BinWidth',1)
title("Deciphered")


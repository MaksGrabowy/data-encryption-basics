clear all
close all

original_file = imread("inputs/a.png");

key = 50;
ascii_original = double(original_file);

ascii_cipher = uint8(mod(double(ascii_original)+key,255));

imwrite(ascii_cipher,"outputs/image_result.png");

%%
file_decipher = imread("outputs/image_result.png");
ascii_result = double(file_decipher);
ascii_decipher = uint8(mod(double(ascii_result)-key,255));

imwrite(ascii_decipher,"outputs/image_deciphered.png");



%%
figure(1)
subplot(1,3,1)
histogram(ascii_original,255,'BinWidth',1)
title("Original")

subplot(1,3,2)
histogram(ascii_cipher,255,'BinWidth',1)
title("ciphered")

subplot(1,3,3)
histogram(ascii_decipher,255,'BinWidth',1)
title("Deciphered")


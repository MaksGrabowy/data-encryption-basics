close all
clear all
load("inputs/hasla.mat")
encrypted = imread("inputs/encrypted_image.png");

column = encrypted(1:255,2);
result = xcorr(column);
figure()
plot(result);

figure()
imshow(encrypted);

figure()
histogram(encrypted,255,"BinWidth",1);

%%
len_of_key = 24;
good_key_count = 1;
for i = 1:length(hasla)
    if(length(hasla{i})==len_of_key)
        pass_mat(good_key_count,:) = double(hasla{i,1});
        good_key_count = good_key_count+1;
    end
end

ascii_encrypted = double(encrypted);

for num = 1:length(pass_mat)
    key_count = 1;
    for i = 1:512
        for j = 1:512
            index = mod(key_count,len_of_key);
            if(index == 0)
                index = len_of_key;
            end
            ascii_decipher(j,i) = uint8(mod(double(ascii_encrypted(j,i))-pass_mat(num,index),255));
            key_count = key_count + 1;
        end
    end
    % result_path = sprintf("outputs/image_%d.png",num);
    % imwrite(ascii_decipher,result_path);
    histo = histogram(ascii_decipher,255,"BinWidth",1);
    differences(num) = max(histo.Values) - min(histo.Values);
end
close all
target = find(differences >= max(differences)-0.1*max(differences));
target_keys = pass_mat(target,:)

clear i
clear j

for num = 1:size(target_keys,1)
    key_count = 1;
    for i = 1:512
        for j = 1:512
            index = mod(key_count,len_of_key);
            if(index == 0)
                index = len_of_key;
            end
            ascii_decipher(j,i) = uint8(mod(double(ascii_encrypted(j,i))-target_keys(num,index),255));
            key_count = key_count + 1;
        end
    end
    result_path = sprintf("outputs/image_to_%d.png",num);
    imwrite(ascii_decipher,result_path);
end

imwrite(ascii_decipher,"outputs/image_decrypted.png");


clear all
close all
load("inputs/hasla.mat")

file = fopen("inputs/encrypted_text.txt","r","n","UTF-8");
original_file = fread(file);
fclose(file);

result = xcorr(original_file);

figure()
plot(result);

%%
len_of_key = 27;
good_key_count = 1;
for i = 1:length(hasla)
    if(length(hasla{i})==len_of_key)
        pass_mat(good_key_count,:) = double(hasla{i,1});
        good_key_count = good_key_count+1;
    end
end


ascii_original = double(original_file);
target_number = 1;

for num = 1:length(pass_mat)
    key_count = 1;

    for i = 1:length(ascii_original)
        index = mod(key_count,len_of_key);
        if(index == 0)
            index = len_of_key;
        end
        ascii_decipher(i) = mod(ascii_original(i)-pass_mat(num,index),255);
        key_count = key_count + 1;
    end
    histo = histogram(ascii_decipher,255,"BinWidth",1);
    differences(num) = max(histo.Values) - min(histo.Values);
    correct = true;
    for j = 1:length(ascii_decipher)
        if(ascii_decipher(j) >= 126)
            correct = false;
        end
    end
    if(correct)
        target_keys(target_number,:) = pass_mat(num,:);
        key_numbers(target_number) = num;
        target_number = target_number + 1;
    end
    clear ascii_decipher
end

close all
target = find(differences == max(differences));
target_key = pass_mat(target,:);

%% finished number is the same as target key!

index = 1;
i = 1;
for i = 1:length(ascii_original)
    index = mod(i,len_of_key);
    if(index == 0)
        index = len_of_key;
    end
    ascii_decrypted(i) = mod(ascii_original(i)-pass_mat(target,index),255);
end

file_result = fopen("outputs/decrypted_text.txt","w","n","UTF-8");
for i = 1:length(ascii_decrypted)
    fwrite(file_result,ascii_decrypted(i));
end

fclose(file_result);


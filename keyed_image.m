clear all
close all

s = rng;
key = randi(255,1,20);

fkey = fopen("inputs/key.txt","w","n","UTF-8");

key_file = fwrite(fkey,key);
fclose(fkey);

key_generated = fopen("inputs/key.txt","r","n","UTF-8");
key_read = fread(key_generated);
fclose(key_generated);

%%
original_file = imread("inputs/a.png");

ascii_original = double(original_file);

key_count = 1;
for i = 1:size(ascii_original,1)
    for j = 1:size(ascii_original,1)
        index = mod(key_count,length(key_read))+1;
        ascii_cipher(i,j) = uint8(mod(double(ascii_original(i,j))+key_read(index),255));
        key_count = key_count + 1;
    end
end

imwrite(ascii_cipher,"outputs/image_result_better.png");

%%
file_decipher = imread("outputs/image_result_better.png");
ascii_result = double(file_decipher);

key_count = 1;
for i = 1:size(ascii_original,1)
    for j = 1:size(ascii_original,1)
        index = mod(key_count,length(key_read))+1;
        ascii_decipher(i,j) = uint8(mod(double(ascii_result(i,j))-key_read(index),255));
        key_count = key_count + 1;
    end
end

imwrite(ascii_decipher,"outputs/image_deciphered_better.png");

%%
figure(1)
subplot(1,3,1)
histogram(ascii_original,255,'BinWidth',1)
title("Original")

subplot(1,3,2)
histogram(ascii_cipher,255,'BinWidth',1)
title("ciphered")

subplot(1,3,3)
histogram(ascii_decipher,255,'BinWidth',1)
title("Deciphered")

